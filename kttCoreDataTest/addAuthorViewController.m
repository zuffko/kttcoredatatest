//
//  addAuthorViewController.m
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "addAuthorViewController.h"
#import "Author.h"

@interface addAuthorViewController ()

@end

@implementation addAuthorViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction) addButtonSelected:(id)sender
{
    //Validate
    if (_nameField.text.length > 0)
    {
        //Create new entity and set properties
        Author *newAuthor = [Author MR_createEntity];
        newAuthor.author_name = _nameField.text;
        
        //Save to persistant storage
        [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreAndWait];
        
        //Inform app
        [[NSNotificationCenter defaultCenter] postNotificationName:@"newPersonAdded" object:nil];
        
        //dismiss view
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please Fill out all fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _nameField.delegate = self;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
