//
//  Author.h
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Book.h"

@class Book;

@interface Author : NSManagedObject

@property (nonatomic) NSString * author_name;
@property (nonatomic) NSManagedObjectID * bookId;
@property (nonatomic) Book *book;


@end
