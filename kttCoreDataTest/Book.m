//
//  Book.m
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "Book.h"

@implementation Book

@dynamic book_name, author, authorId;

@end
