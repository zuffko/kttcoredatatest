//
//  BooksViewController.h
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Author.h"
#import "Book.h"
#import <CoreData/CoreData.h>

@interface BooksViewController : UITableViewController
@property (strong, nonatomic) id detailItem;
@property (nonatomic, retain) Author *currentAuthor;
@property (nonatomic, retain) NSMutableArray* books;
@end
