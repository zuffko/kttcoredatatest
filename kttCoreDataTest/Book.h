//
//  Book.h
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Author.h"
@class Author;

@interface Book : NSManagedObject

@property (nonatomic) NSString *book_name;
@property (nonatomic) NSManagedObjectID * authorId;
@property (nonatomic) Author *author;

@end
