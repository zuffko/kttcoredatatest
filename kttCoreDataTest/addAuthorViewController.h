//
//  addAuthorViewController.h
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface addAuthorViewController : UIViewController <UITextFieldDelegate>{
        IBOutlet UITextField *_nameField;
}


-(IBAction) addButtonSelected:(id)sender;

@end
