//
//  BooksViewController.m
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "BooksViewController.h"
#import "AddBookViewController.h"
#import <CoreData/CoreData.h>

@interface BooksViewController (){
    
    NSString *qwe;
}

@end

@implementation BooksViewController
@synthesize currentAuthor;

- (void)viewDidLoad
{
    [super viewDidLoad];
    _books = [[NSMutableArray alloc] init];
    [self refreshData];
    NSLog(@"books %@", _books);
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationNewBookAdded:) name:@"newBookAdded" object:nil];
}

-(void) notificationNewBookAdded:(NSNotification*)notification
{
    
    [self refreshData];
}

-(void) refreshData
{
    //_books=nil;
    NSManagedObjectContext *context = currentAuthor.managedObjectContext;
    NSLog(@"currentAuthor.managedObjectContext %@", currentAuthor);
    NSLog(@"count %i", [[Book MR_findAll] count]);
    
    
    
    NSArray *loadBooks = [Book MR_findAllSortedBy:@"author" ascending:YES inContext:context];
    _books = [loadBooks mutableCopy];
//    if ([loadBooks count]>0) {
//    int n=0;
//    while (n<=[loadBooks count]) {
//        Book *qweqwe=[loadBooks objectAtIndex:n];
//        if (qweqwe.authorId==[currentAuthor objectID]) {
//            [_books addObject:qweqwe];
//            n++;
//        }
//    }
//    }
     //[qweqwe.author objectID];
    //_books initWithArray:[currentAuthor.book ]
    //_books = [Book obke]];
   //_books = [[NSMutableArray alloc] initWithArray:[self.currentAuthor.book.zone allObjects]];
    
    [self.tableView reloadData];
    context=nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_books count]>0) {
        return [_books count];
    }else{
    return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cellq";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    if ([_books count]>0) {
        
        //NSLog(@"%@", currentAuthor);
        Book *currentBook = [_books objectAtIndex:indexPath.row];
        cell.textLabel.text = currentBook.book_name;
        [currentBook objectID];
    }
    

    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[_books objectAtIndex:indexPath.row] MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreAndWait];
        [self refreshData];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([[segue identifier] isEqualToString:@"addBook"]) {
        AddBookViewController *addBookViewController = segue.destinationViewController;
        addBookViewController.currentAuthor=currentAuthor;
    }
}

@end
