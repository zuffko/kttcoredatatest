//
//  AddBookViewController.h
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
#import "Author.h"
#import "Key.h"
#import <CoreData/CoreData.h>

@interface AddBookViewController : UIViewController<UITextFieldDelegate>{
    IBOutlet UITextField *_nameField;
    
}
@property (nonatomic) Author *author;
@property (nonatomic) Author *currentAuthor;

-(IBAction) addButtonSelected:(id)sender;


@end
