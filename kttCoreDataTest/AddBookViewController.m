//
//  AddBookViewController.m
//  kttCoreDataTest
//
//  Created by zufa on 26/6/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "AddBookViewController.h"

@interface AddBookViewController ()

@end

@implementation AddBookViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _nameField.delegate = self;
}

-(IBAction) addButtonSelected:(id)sender
{
    if (_nameField.text.length > 0)
    {
        Book *newBook = [Book MR_createEntity];
        newBook.book_name = _nameField.text;
        newBook.author = _currentAuthor;
        // NSString*identifier=[[_currentAuthor objectID] ];
        //newBook.authorId = [_currentAuthor objectID];
        

        [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreAndWait];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"newBookAdded" object:nil];

        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please Fill out all fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
